package codeJam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class TrainCars {

	private List<ArrayList<String>> tasks = new ArrayList<ArrayList<String>>();

	private List<String> trainParts;

	int numberOfTasks, currentTask = 0;

	public TrainCars(File fin) throws IOException {
		FileInputStream fis = new FileInputStream(fin);

		BufferedReader br = new BufferedReader(new InputStreamReader(fis));

		String line = null;
		line = br.readLine();
		numberOfTasks = Integer.parseInt(line);
		System.err.println("Anzahl der Aufgaben: " + numberOfTasks);

		int counter = 0;
		while ((line = br.readLine()) != null) {
			String temp = line;
			line = br.readLine();
			System.err.println("Aufgabe: " + (++counter) + "; " + temp
					+ " Zugstücke: " + line);
			String[] parts = line.split(" ");
			tasks.add(new ArrayList<String>(Arrays.asList(parts)));
		}

		currentTask = 0;
		br.close();
	}

	void sort() {

		for (List<String> a : tasks) {
			Collections.sort(a);
		}
	}

	int getNumberOfTasks() {
		return numberOfTasks;
	}

	long solve(int currentTask) {
		trainParts = tasks.get(currentTask);


		if (trainParts.size() == 0) {
			return 0;
		}

		HashSet<String> letterUsedAsOnlyChar = new HashSet<String>();
		HashSet<String> letterUsedAsFirstChar = new HashSet<String>();
		HashSet<String> letterUsedAsLastChar = new HashSet<String>();
		HashSet<String> letterUsedAsIslandChar = new HashSet<String>();

		ArrayList<String> relevanteZuege = new ArrayList<String>();

		// Die folgende Schleife dient nur dazu, "unmögliche Aufgaben" durch
		// Inseln oder zu viele erste / letzte Buchstaben zu erkennen sowie die
		// relevanten Daten neu zusammenzufassen.
		for (int i = 0; i < trainParts.size(); ++i) {
			String part = trainParts.get(i);
			char currentLetter = part.charAt(0);
			String currentStr = String.valueOf(currentLetter);

			if (part.matches(currentLetter + "*")) {
				// dieser Fall ist mit fast allen situationen verträglich.

				if (letterUsedAsIslandChar.contains(currentStr)) {
					// in diesem Fall ist unsere Aufgabe unmöglich
					return 0;
				}

				letterUsedAsOnlyChar.add(currentStr); // davon darf es ganz
														// viele geben!
				relevanteZuege.add(currentStr);
			} else {
				// in diesem fall fängt part "nur" mit unserem char an!
				if (!letterUsedAsFirstChar.add(currentStr)) {
					// der letter wurde schon einmal als erster verwendet ->
					// Unmögliche Aufgabe
					return 0;
				}

				for (int j = 1; j < part.length(); j++) {
					if (currentLetter == part.charAt(j)) {
						continue; // uninteressant
					} else {
						if (part.indexOf(currentLetter, j) != -1) {
							// currentLetter kommt in diesem Part mehrmals vor,
							// aber mit
							// Unterbrechnung!
							return 0;
						}

						currentLetter = part.charAt(j); // neuer currentLetter
						currentStr = String.valueOf(currentLetter);

						if (part.substring(j).matches(currentLetter + "*")) {
							// mit diesem neuen currentLetter hört unser part
							// auch auf
							if (!letterUsedAsLastChar.add(currentStr)) { // <-
																			// Hinzufügen
								// der letter wurde schon einmal als letzter
								// verwendet -> Unmögliche Aufgabe
								return 0;
							}
							if (letterUsedAsIslandChar.contains(currentStr)) {
								// Unmögliche Aufgabe, der Letter ist wo anders
								// eine Insel.
								return 0;
							}

							// dieser Zug -Teil ist relevant. Alle zulässigen
							// Inseln interessieren uns für die Verbindungen
							// später nicht. Homogene Zuggruppen auch nicht.
							relevanteZuege.add(String.valueOf(part.charAt(0)
									+ currentStr));

						} else {
							if (!letterUsedAsIslandChar.add(currentStr)) { // <-
																			// Hinzufügen
								// der letter wurde schon einmal als insel
								// verwendet -> Unmögliche Aufgabe
								return 0;
							}
							if (letterUsedAsFirstChar.contains(currentStr)
									|| letterUsedAsLastChar
											.contains(currentStr)
									|| letterUsedAsOnlyChar
											.contains(currentStr)) {
								// Insel vertägt sich mit nichts! Unmögliche
								// Aufgabe!
								return 0;
							}
						}
					}
				}
			}

		}
		
		if (trainParts.size() == 1) {
			return 1;
		}

		Collections.sort(relevanteZuege);

		HashMap<String, Integer> joker = new HashMap<String, Integer>();
		HashMap<String, String> partsByFirst = new HashMap<String, String>();
		HashMap<String, String> partsBySecond = new HashMap<String, String>();

		// Daten vorbereiten
		for (int i = 0; i < relevanteZuege.size(); ++i) {
			String part = relevanteZuege.get(i);
			String key = part.substring(0, 1);
			if (isHomogen(part)) {
				if (joker.containsKey(key)) {
					joker.put(key, joker.get(key) + 1); // 1 hochzählen
				} else {
					joker.put(key, 1); // initialisierung
				}
			} else {
				partsByFirst.put(key, part);
				partsBySecond.put(part.substring(1), part);
			}
		}

		// geeigneten Start finden
		Stack<String> starters = new Stack<String>();
		for (Map.Entry<String, String> entry : partsByFirst.entrySet()) {
			String key = entry.getKey();
			if (!partsBySecond.containsKey(key)) {
				starters.push(key);
			}
		}

		if (starters.size() == 0 && partsByFirst.size() != 0) {
			// dann haben wir ein echtes Problem, die Aufgabe ist unmöglich.
			return 0;
		}
		String currentLetter = null;
		if (starters.size() != 0) {
			currentLetter = starters.pop();
		} else {
			currentLetter = joker.entrySet().iterator().next().getKey();
		}

		ArrayList<String> carsInOrder = new ArrayList<String>();
		while (carsInOrder.size() < relevanteZuege.size()) {
			// zunächst prüfen, ob ein Joker rein muss.
			if (joker.containsKey(currentLetter)) {
				int num = joker.get(currentLetter);
				for (int i = 0; i < num; ++i) {
					carsInOrder.add(currentLetter);
				}
				joker.remove(currentLetter); // aufräumen
			}
			if (partsByFirst.size() > 0) {
				String part = partsByFirst.get(currentLetter);
				carsInOrder.add(part);
				partsByFirst.remove(currentLetter);
				// partsBySecond.remove(part.substring(1));
				currentLetter = part.substring(1);
			}
			if (!partsByFirst.containsKey(currentLetter)) {
				// schnell alle verbleibenden hinzufügen
				if (joker.containsKey(currentLetter)) {
					int num = joker.get(currentLetter);
					for (int i = 0; i < num; ++i) {
						carsInOrder.add(currentLetter);
					}
					joker.remove(currentLetter);
				}
				if (starters.size() > 0) {
					currentLetter = starters.pop();
				} else {
					for (Map.Entry<String, Integer> entry : joker.entrySet()) {
						currentLetter = entry.getKey();
						int num = joker.get(currentLetter);
						for (int i = 0; i < num; ++i) {
							carsInOrder.add(currentLetter);
						}
					}
				}
			}
		}

		// System.out.println("cars in order: " + carsInOrder);

		return compute(carsInOrder);
	}

	boolean isHomogen(String str) {
		char currentChar = str.charAt(0);
		for (int i = 0; i < str.length(); ++i) {
			if (str.charAt(i) != currentChar) {
				return false;
			}
		}
		return true;
	}

	public static long factorial(long n) {
		if (n <= 1) {
			return 1;
		} else {
			return n * factorial(n - 1);
		}
	}

	long compute(ArrayList<String> carsInOrder) {

		long len = carsInOrder.size();

		/*
		 * (aa aaa a aaa a)-ab ... Die ersten Strings können innerhalb des
		 * Verbands beliebig getauscht werden, "ab" nicht. ab-bc ... Die Strings
		 * können nicht getauscht werden. ab df ... Die beiden Strings können
		 * beliebig getauscht werden.
		 * 
		 * Bsp.: (aa a aaaa) ee-ef-fff lk tp gh-hh Also: x y lk tp z
		 * 
		 * k = (n - (#Strings, die in einem Verbund sind) + #Verbünde)! *
		 * Produkt(#Strings pro loser Verbund)
		 */

		List<Integer> loseVerbuende = new ArrayList<Integer>();
		List<Integer> festeVerbuende = new ArrayList<Integer>();

		int bufferLV = 1, bufferFV = 1;
		for (int i = 0; i < len - 1; ++i) {
			String currentString = carsInOrder.get(i);
			if (currentString.equals(carsInOrder.get(i + 1))) {
				if (bufferFV > 1) {
					festeVerbuende.add(bufferFV);
					bufferFV = 1;
				}
				bufferLV++;
			} else if (currentString.charAt(currentString.length() - 1) == carsInOrder
					.get(i + 1).charAt(0)) {
				if (bufferLV > 1) {
					loseVerbuende.add(bufferLV);
					bufferLV = 1;
				}
				bufferFV++;
			} else {
				if (bufferLV > 1) {
					loseVerbuende.add(bufferLV);
					bufferLV = 1;
				} else if (bufferFV > 1) {
					festeVerbuende.add(bufferFV);
					bufferFV = 1;
				}
			}
		}
		if (bufferLV > 1) {
			loseVerbuende.add(bufferLV);
			bufferLV = 1;
		} else if (bufferFV > 1) {
			festeVerbuende.add(bufferFV);
			bufferFV = 1;
		}

		long product = 1;
		long sum = 0;
		for (int i = 0; i < loseVerbuende.size(); ++i) {
			product *= factorial(loseVerbuende.get(i));
			sum += loseVerbuende.get(i) - 1;
		}

		for (int i = 0; i < festeVerbuende.size(); ++i) {
			sum += festeVerbuende.get(i) - 1;
		}

		// System.out.println(carsInOrder);
		// System.out.println("obere Ebene: " + (len - sum));
		// System.out.println("untere Ebene (Fakultät): " + product);
		// System.out.println("loseVerbünde: " + loseVerbuende);
		// System.out.println("festeVerbünde: " + festeVerbuende);

		return (factorial(len - sum) % 1000000007) * (product % 1000000007);
	}

	public static void main(String[] args) throws IOException {

		// 0. Initialisierung.
		File fin = new File("res/test1.txt");
		TrainCars TrainCars = new TrainCars(fin);

		// 1. Sortieren der Zeichenketten aller Aufgaben.
		// trainCars.sort();

		int numberOfTasks = TrainCars.getNumberOfTasks();
		for (int i = 0; i < numberOfTasks; ++i) {
			System.out.println("Case #" + (i + 1) + ": " + TrainCars.solve(i));
		}
	}

}
